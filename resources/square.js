/**
 * Created by esterytterbrink on 17/02/2014.
 */
function SudokuSquare(initialPossibleValues){
    var possibleValues = initialPossibleValues || [1,2,3,4,5,6,7,8,9];

   var propagateResolutionFunction = function(resolvedValue){

   };

    function setResolvedValue(resolvedValue){
        possibleValues = [resolvedValue];
        self.propagateResolutionFunction(resolvedValue);
    };

    function getPossibleValues(){
        return possibleValues;
    };

    function resolvedValue(){
        if(isResolved()){
            return possibleValues[0];
        }else{
            return false;
        }
    };

    function isResolved(){
        if(possibleValues.length > 1){
            return false;
        }else{
            return true;
        }
    };

    function excludeValue(valueToExclude){
        if(_.contains(possibleValues, valueToExclude)){
            possibleValues = _.without(possibleValues, valueToExclude);
            if(isResolved()){
                self.propagateResolutionFunction(resolvedValue());
            }
        }
    };

    var self =  {
        getPossibleValues:getPossibleValues,
        setResolvedValue:setResolvedValue,
        excludeValue:excludeValue,
        isResolved:isResolved,
        resolvedValue:resolvedValue,
        propagateResolutionFunction:propagateResolutionFunction
    };
    return self;
}

function createSudokuSquaresFromArrayOfNumbers(values){
    var squares = [];
    for(var i = 0; i < values.length; i++){
        var square;
        if(values[i]<1){
            square = SudokuSquare();
        }else{
           square = SudokuSquare([values[i]]);

        }
        squares.push(square);
    }
    return squares;
}