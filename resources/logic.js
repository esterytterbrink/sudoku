/**
 * Created by esterytterbrink on 17/02/2014.
 */
function SudokuLogic(){

    function findOnlyPossibleInGroup(squares){
        var foundOnlyPossible = [];
        for(var i = 1; i<10; i++){
            var containsNumber = _.filter(squares, function(square){
                return (_.indexOf(square.getPossibleValues(),i, true ) != -1);
            });
            if(containsNumber.length === 1){
                var foundSquare = containsNumber[0];
                if(!foundSquare.isResolved()){
                    foundSquare.setResolvedValue(i);
                    foundOnlyPossible.push(foundSquare);
                }
            }
        }
        return foundOnlyPossible;
    }

    function removeValuesFromFirstGroupThatIsOnlyExistentInSecondGroupWithinUnion(firstGroup, secondGroup, union){
        var exclusiveValuesForRow = findNumbersExistingInSmallGroupButNotInLargeGroup(union, secondGroup);
        return removeValuesFromSquaresReturnIfChangedAtLeastOne(exclusiveValuesForRow, firstGroup);
    }

    function removeValuesFromSquaresReturnIfChangedAtLeastOne(values, squares){
        var changedAtLeastOne = false;
        for(var i = 0; i<values.length; i++){
            squares.forEach(function(square){
                if(!square.isResolved()){
                    if(_.indexOf(square.getPossibleValues(),values[i], true ) != -1){
                        changedAtLeastOne = true;
                        square.excludeValue(values[i]);
                    }
                }
            });
        }
        return changedAtLeastOne;
    }

    function getUniquePossibleValuesFromGroup(squares){
        console.log("squares.length " + squares.length);

        var totalAmountOfNumbers  = [];
        for(var i = 0; i<squares.length; i++){
            console.log("squares[i].getPossibleValues() " + squares[i].getPossibleValues());
            totalAmountOfNumbers = _.union(totalAmountOfNumbers, squares[i].getPossibleValues());
        }
        console.log("totalAmountOfNumbers " + totalAmountOfNumbers);

        return totalAmountOfNumbers.sort(function(a,b){
            if (a < b){
                return -1;
            }
            if (a > b){
                return 1;
            }
            return 0;
        });

    }
    function findIfSquaresAndFiguresMap(squares){
        var totalAmountOfNumbers = getUniquePossibleValuesFromGroup(squares);

        if(totalAmountOfNumbers.length === squares.length){
            return totalAmountOfNumbers;
        }
        return [];
    }
    function findNumbersExistingInSmallGroupButNotInLargeGroup(smallGroup, largeGroup){
        var numbersInSmall = getUniquePossibleValuesFromGroup(smallGroup);
        var numbersInLarge = getUniquePossibleValuesFromGroup(largeGroup);
        var uniqueInSmall = [];
        for(var i = 0; i<numbersInSmall.length; i++){
            if(_.indexOf(numbersInLarge, numbersInSmall[i], true)===-1){
                uniqueInSmall.push(numbersInSmall[i]);
            }
        }
        return uniqueInSmall;
    }

    return{
        removeValuesFromSquaresReturnIfResolvedAtLeastOne:removeValuesFromSquaresReturnIfChangedAtLeastOne,
        findOnlyPossibleInGroup:findOnlyPossibleInGroup,
        findIfSquaresAndFiguresMap:findIfSquaresAndFiguresMap,
        findNumbersExistingInSmallGroupButNotInLargeGroup:findNumbersExistingInSmallGroupButNotInLargeGroup,
        removeValuesFromFirstGroupThatIsOnlyExistentInSecondGroupWithinUnion:removeValuesFromFirstGroupThatIsOnlyExistentInSecondGroupWithinUnion
    }
};