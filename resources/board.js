/**
 * Created by esterytterbrink on 15/02/2014.
 */
function SudokuBoard(arrayWithNumbers){

    var squaresAsArray = createSudokuSquaresFromArrayOfNumbers(arrayWithNumbers);

    squaresAsArray.forEach(function(square, index, array){
        square.propagateResolutionFunction = updateFunctionForSquareInBoard(index, array);
    });

    squaresAsArray.forEach(function(square, index, array){
        if(square.isResolved()){
            square.propagateResolutionFunction(square.resolvedValue());
        }
    });


    function arrayRepresentation(){
        var arrayWithNumbers = [];
        squaresAsArray.forEach(function(square) {
            if (square.isResolved()) {
                arrayWithNumbers.push(square.resolvedValue());
            } else {
                arrayWithNumbers.push(0);
            }
        });
        return arrayWithNumbers;
    };

    function squaresForRow(row){
        var squares = [];
        for(var i = row*9; i<(row +1)*9; i++){
            squares.push(squaresAsArray[i]);
        }
        return squares;
    }

    function squaresForColumn(col){
        var squares = [];
        for(var i = col; i<81; i+=9){
            squares.push(squaresAsArray[i]);
        }
        return squares;
    }

    function startIndexForQuadrant(quadrant){
        var column = (quadrant%3) *3;
        var row = Math.floor(quadrant/3) * 27;
        return  column + row;
    }

    function squaresForQuadrant(quadrant){
        var squares = [];
        var startIndex = startIndexForQuadrant(quadrant);
        for(var i = startIndex; i<startIndex + 21; i++){
            squares.push(squaresAsArray[i]);
            if(squares.length%3 === 0){
                i+=6;
            }
        }
        return squares;
    }
    function subcolInQuadrant(col, quadrant){
        var allSquares = squaresForQuadrant(quadrant);
        var subcol = [];
        for(var i = col; i<9; i+=3){
            subcol.push(allSquares[i]);
        }
        return subcol;
    }
    function subrowInQuadrant(row, quadrant){
        var allSquares = squaresForQuadrant(quadrant);
        var subrow= [];
        for( var i = row*3; i<(row+1)*3; i++){
            subrow.push(allSquares[i]);
        }
        return subrow;
    }
    function restOfRowExceptSubpart(row, subpart){
        var squares = squaresForRow(row);
        var restOfRow = [];
        for(var i = 0; i<9; i++){
            if(Math.floor(i/3)!=subpart){
                restOfRow.push(squares[i]);
            }
        }
        return restOfRow;
    }

    function restOfColExceptSubpart(col, subpart){
        var squares = squaresForColumn(col);
        var restOfCol = [];
        for(var i = 0; i<9; i++){
            if(Math.floor(i/3)!=subpart){
                restOfCol.push(squares[i]);
            }
        }
        return restOfCol;
    }

    function restOfQuadrantExceptOfSubrow(quadrant, subrow){
        var squares = squaresForQuadrant(quadrant);
        var restOfQuadrant = [];
        for(var i = 0; i<9; i++){
            if(Math.floor(i/3)!=subrow){
                restOfQuadrant.push(squares[i]);
            }
        }
        return restOfQuadrant;
    }

    function restOfQuadrantExceptOfSubcol(quadrant, subcol){
        var squares = squaresForQuadrant(quadrant);
        var restOfQuadrant = [];
        for(var i = 0; i<9; i++){
            if(i%3 != subcol){
                restOfQuadrant.push(squares[i]);
            }
        }
        return restOfQuadrant;
    }

    function rowIndexForRowIndexInQuadrant(rowIndex, quadrant){
        var quadrantRow = Math.floor(quadrant/3);
        return quadrantRow * 3 + rowIndex;
    }

    function columnIndexForColumnIndexInQuadrant(columIindex,quadrant){
        var quadrantColumn = quadrant%3;
        return quadrantColumn*3+ columIindex;
    }

    function applyRules(){
        var changed = false;
        var changedAtLeastOne = false;
        var logic = SudokuLogic();
        var rounds = 0;
        do{
            changed = false;
            rounds ++;
            for(var i = 0; i<9; i++){
                var rows = squaresForRow(i);
                var cols = squaresForColumn(i);
                var quadrants = squaresForQuadrant(i);
                changed = changed || logic.applyRulesToGroupSquares(rows);
                changed = changed || logic.applyRulesToGroupSquares(cols);
                changed = changed || logic.applyRulesToGroupSquares(quadrants);
                if(changed === true){
                    changedAtLeastOne = true;
                    console.log("changedAtLeastOne set to true");
                }
                console.log("Apply rules Round " + rounds + " result " + reduceGroupToString(squaresAsArray));

            }
        }while(changed);
        return changedAtLeastOne;
    }
    function solve(){
        var round = 0;
        do{
            round++;
          //  var changedByRules = applyRules();
            var changedByGroup = applyOnlyInGroup();
            var changedBySubgroup = applyMustBeInSubGroup();
            console.log("Solve Round " + round + " result " + reduceGroupToString(squaresAsArray));

        }while((changedByGroup || changedBySubgroup) && round<10);

    }


    function applyMustBeInSubGroup(){
        var changedThisRound = false;
        var logic = SudokuLogic();
        for(var quadrant = 0; quadrant<9; quadrant++){
            for(var subRow = 0; subRow<3; subRow++){
                var squaresForSubrow = subrowInQuadrant(subRow, quadrant);
                var rowForSubrow = rowIndexForRowIndexInQuadrant(subRow, quadrant);
                var restOfSquaresInQuadrant1 = restOfQuadrantExceptOfSubrow(quadrant, subRow);
                var restOfSquaresInRow = restOfRowExceptSubpart(rowForSubrow, subRow);
                var valuesThatMustBeInSubgroup1 = logic.findIfSquaresAndFiguresMap(squaresForSubrow);

                changedThisRound = changedThisRound || logic.removeValuesFromSquaresReturnIfResolvedAtLeastOne(valuesThatMustBeInSubgroup1,restOfSquaresInQuadrant1);
                changedThisRound = changedThisRound || logic.removeValuesFromSquaresReturnIfResolvedAtLeastOne(valuesThatMustBeInSubgroup1, restOfSquaresInRow);

                changedThisRound = changedThisRound || logic.removeValuesFromFirstGroupThatIsOnlyExistentInSecondGroupWithinUnion(restOfSquaresInQuadrant1, restOfSquaresInRow,squaresForSubrow);
                changedThisRound = changedThisRound || logic.removeValuesFromFirstGroupThatIsOnlyExistentInSecondGroupWithinUnion(restOfSquaresInRow,restOfSquaresInQuadrant1,squaresForSubrow);
            }
            for(var subCol=0; subCol<3; subCol++){
                var squaresForSubcol = subcolInQuadrant(subCol, quadrant);
                var colForSubcol = columnIndexForColumnIndexInQuadrant(subCol, quadrant);
                var restOfSquaresInQuadrant = restOfQuadrantExceptOfSubcol(quadrant, subCol);
                var restOfSquaresInCol = restOfRowExceptSubpart(colForSubcol, subCol);

                var valuesThatMustBeInSubgroup = logic.findIfSquaresAndFiguresMap(squaresForSubcol);
                changedThisRound = changedThisRound || logic.removeValuesFromSquaresReturnIfResolvedAtLeastOne(valuesThatMustBeInSubgroup, restOfSquaresInQuadrant);
                changedThisRound = changedThisRound || logic.removeValuesFromSquaresReturnIfResolvedAtLeastOne(valuesThatMustBeInSubgroup, restOfSquaresInCol);

                changedThisRound = changedThisRound || logic.removeValuesFromFirstGroupThatIsOnlyExistentInSecondGroupWithinUnion(restOfSquaresInQuadrant, restOfSquaresInCol,squaresForSubcol);
                changedThisRound = changedThisRound || logic.removeValuesFromFirstGroupThatIsOnlyExistentInSecondGroupWithinUnion(restOfSquaresInCol,restOfSquaresInQuadrant,squaresForSubcol);

            }
        }
        return changedThisRound;
    }

    function applyOnlyInGroup(){
        var changed = false;
        var cnangedAtLeastOne = false;
        var logic = SudokuLogic();
        var rounds = 0;
        do{
            changed = false;
            var foundInRound = [];
            rounds ++;
            for(var i = 0; i<9; i++){
                var rows = squaresForRow(i);
                var cols = squaresForColumn(i);
                var quadrants = squaresForQuadrant(i);
                console.log("Start test rows");
                var foundInRows = logic.findOnlyPossibleInGroup(rows);
                var foundInCols = logic.findOnlyPossibleInGroup(cols);
                var foundInQuadrants = logic.findOnlyPossibleInGroup(quadrants);
                var allFound = foundInRows.concat(foundInCols).concat(foundInQuadrants);
                foundInRound = foundInRound.concat(allFound);
                if(allFound.length >0){
                    changed = true;
                    cnangedAtLeastOne = true;
                }
            }
            console.log("Apply only in group Round " + rounds + " result " + reduceGroupToString(foundInRound));
        }while(changed);
        return cnangedAtLeastOne;
    }

    return{
        arrayRepresentation:arrayRepresentation,
        squaresForColumn:squaresForColumn,
        squaresForRow:squaresForRow,
        squaresForQuadrant:squaresForQuadrant,
        subcolInQuadrant:subcolInQuadrant,
        subrowInQuadrant:subrowInQuadrant,
        restOfRowExceptSubpart:restOfRowExceptSubpart,
        restOfColExceptSubpart:restOfColExceptSubpart,
        restOfQuadrantExceptOfSubrow:restOfQuadrantExceptOfSubrow,
        restOfQuadrantExceptOfSubcol:restOfQuadrantExceptOfSubcol,
        rowIndexForRowIndexInQuadrant:rowIndexForRowIndexInQuadrant,
        columnIndexForColumnIndexInQuadrant:columnIndexForColumnIndexInQuadrant,
        applyRules:applyRules,
        startIndexForQuadrant:startIndexForQuadrant,
        squaresAsArray:squaresAsArray,
        solve:solve
    }
};
