/**
 * Created by esterytterbrink on 17/02/2014.
 */
/**
 * Created by esterytterbrink on 17/02/2014.
 */

function rowFilterForSquare(squareIndex){
    return function(value, index, array){
        if(index === squareIndex){
            return false;
        }else if(Math.floor(squareIndex/9) === Math.floor(index/9)){
            return true;
        }else{
            return false;
        }
    };
}

function columnFilterForSquare(squareIndex){
    return function(value, index, array)
    {
        if(index === squareIndex){
            return false;
        }else if(squareIndex%9 === index%9){
            return true;
        }else{
            return false;
        }

    };
}


function quadrantFilterForSquare(squareIndex){

    var firstIndexInBigRow = Math.floor(squareIndex/27)*27;
    var firstCol = Math.floor((squareIndex%9)/3)*3;
    return function(value, index, array){
        var inBigRow = (index >= firstIndexInBigRow) && (index < (firstIndexInBigRow + 27));
        var inOneOfTheCols = (index%9 === firstCol || index%9 === (firstCol+1) || index%9===(firstCol+2));
        return(inBigRow && inOneOfTheCols && (!(index===squareIndex)));
    };
}

function updateSquaresWithValue(squares, value){
    squares.forEach(function(square, index, array){
        square.excludeValue(value);
    });
}

function updateFunctionForSquareInBoard(squareIndex, board){
    var rowSquares = board.filter(rowFilterForSquare(squareIndex));
    var columSquares = board.filter(columnFilterForSquare(squareIndex));
    var quadrantSquares = board.filter(quadrantFilterForSquare(squareIndex));
    return function(confirmedValue){
        updateSquaresWithValue(rowSquares.concat(columSquares).concat(quadrantSquares), confirmedValue);

    }
}