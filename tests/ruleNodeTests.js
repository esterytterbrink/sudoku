
test("Test get rest of row for imdex", function(){
    var filterdlist = _.range(80).filter(rowFilterForSquare(15));
    var facit = [9,10,11, 12, 13, 14,16,17];
    ok(filterdlist.toString() === facit.toString(), filterdlist.toString());
});

test("Test get rest of column for index", function(){
    var filterdlist = _.range(80).filter(columnFilterForSquare(1));
    var facit = [10,19,28,37,46,55,64,73];
    ok(filterdlist.toString() === facit.toString(), filterdlist.toString());
});

test("Test get rest of quadrant for index", function(){
   var filterdlist = _.range(80).filter(quadrantFilterForSquare(44));
    var facit = [33,34,35,42,43,51,52,53];
    ok(filterdlist.toString() === facit.toString(), filterdlist.toString());
});