/**
 * Created by esterytterbrink on 15/02/2014.
 */

test("init empty square", function(){
    var square = SudokuSquare();
    ok(square.getPossibleValues().toString() == [1,2,3,4,5,6,7,8,9].toString(),square.getPossibleValues());

});

test("init defined square", function(){
   var square = SudokuSquare([8]);
    ok(square.getPossibleValues().toString() == "8", square.getPossibleValues());

});

test("set resolved value", function(){
    var square = SudokuSquare();
    square.setResolvedValue(5);
    ok(square.getPossibleValues().toString()== "5");

});

test("exclude value", function(){
    var square = SudokuSquare();
    square.excludeValue(5);
    ok(square.getPossibleValues().toString() == [1,2,3,4,6,7,8,9].toString(), square.getPossibleValues().toString() );

});

test("is not resolved", function(){
   var square = SudokuSquare([5, 6]);
   ok(square.isResolved() === false, square.getPossibleValues().toString());
});

test("is resolved", function(){
    var square = SudokuSquare([5,6]);
    square.excludeValue(6);
    ok(square.isResolved() === true, square.getPossibleValues().toString());

});

test("Create Squares from array", function(){
    var squares = createSudokuSquaresFromArrayOfNumbers([5,0,1,9]);
    ok(squares[0].isResolved() === true, squares[0].getPossibleValues().toString());
    ok(squares[1].isResolved() === false, squares[1].getPossibleValues().toString());
    ok(squares[2].isResolved() === true, squares[2].getPossibleValues().toString());
    ok(squares[3].isResolved() === true, squares[3].getPossibleValues().toString());
    ok(squares.length === 4, "There should be four squares");

});