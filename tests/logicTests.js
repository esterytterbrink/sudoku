/**
 * Created by esterytterbrink on 16/02/2014.
 */



/**test("apply rules — one missing", function(){
    var squares = [SudokuSquare([1]),SudokuSquare([2]),SudokuSquare([3]),SudokuSquare([4]),SudokuSquare([5]),SudokuSquare([6]),SudokuSquare(),SudokuSquare([8]),SudokuSquare([9])];
    var logic = SudokuLogic();
    var changed = logic.applyRulesToGroupSquares(squares);
    var solvedGroup = reduceGroupToString(squares);
    ok( solvedGroup === "123456789", solvedGroup );
});

test("apply rules — two missing", function(){
    var squares = [SudokuSquare([1]),SudokuSquare([2]),SudokuSquare([3]),SudokuSquare([4]),SudokuSquare([5]),SudokuSquare([6, 8]),SudokuSquare(),SudokuSquare([8]),SudokuSquare([9])];
    var logic = SudokuLogic();
    var changed = logic.applyRulesToGroupSquares(squares);
    var solvedGroup = reduceGroupToString(squares);
    ok( solvedGroup === "123456789", solvedGroup );
});

test("apply rules — three missing", function(){
    var squares = [SudokuSquare([1]),SudokuSquare([2]),SudokuSquare([3,4,5]),SudokuSquare([4,6]),SudokuSquare([5]),SudokuSquare([6, 8]),SudokuSquare(),SudokuSquare([8]),SudokuSquare([9])];
    var logic = SudokuLogic();
    var changed = logic.applyRulesToGroupSquares(squares);
    var solvedGroup = reduceGroupToString(squares);
    ok( solvedGroup === "123456789", solvedGroup );
});*/

test("Number of possible figures equal number squares - true", function(){
    var squares = [SudokuSquare([1, 2]), SudokuSquare([1,4]), SudokuSquare([2,4])];
    var logic = SudokuLogic();
    var numbersUsed = logic.findIfSquaresAndFiguresMap(squares);
    ok(numbersUsed.toString()===[1,2,4].toString(), numbersUsed);
});

test("Number of possible figures equal number squares - false", function(){
    var squares = [SudokuSquare([1, 2, 5]), SudokuSquare([1,4]), SudokuSquare([2,4])];
    var logic = SudokuLogic();
    var numbersUsed = logic.findIfSquaresAndFiguresMap(squares);
    ok(numbersUsed.length === 0, numbersUsed);
});

test("find only possible in group", function(){
    var squares = [SudokuSquare([1,8]),SudokuSquare([2,3,8]),SudokuSquare([4,5]),SudokuSquare([4,6]),SudokuSquare([5,7]),SudokuSquare([6, 8]),SudokuSquare([2,4]),SudokuSquare([7,8]),SudokuSquare([9])];
    var logic = SudokuLogic();
    var found = logic.findOnlyPossibleInGroup(squares);
    var solved = reduceGroupToString(found);
    ok(solved === "13", solved);
});

test("subgroup contains values that does not exist in larger group — 1 */", function(){
   var smallerGroup = [SudokuSquare([1,2]), SudokuSquare([1,2,8]), SudokuSquare([1,5])];
   var largerGroup = [SudokuSquare([2,3]), SudokuSquare(2,5),SudokuSquare(2,4),SudokuSquare(2,6),SudokuSquare(2,7), SudokuSquare([2,3,4,5,6,7,8,9])];
    var logic = SudokuLogic();
    var result = logic.findNumbersExistingInSmallGroupButNotInLargeGroup(smallerGroup, largerGroup);
    ok(result[0]===1 && result.length == 1, result);
});
test("subgroup contains values that does not exist in larger group — 0 */", function(){
    var smallerGroup = [SudokuSquare([1,2]), SudokuSquare([1,2,8]), SudokuSquare([1,5])];
    var largerGroup = [SudokuSquare([1,2,3]), SudokuSquare(2,5),SudokuSquare(2,4),SudokuSquare(2,6),SudokuSquare(2,7), SudokuSquare([2,3,4,5,6,7,8,9])];
    var logic = SudokuLogic();
    var result = logic.findNumbersExistingInSmallGroupButNotInLargeGroup(smallerGroup, largerGroup);
    ok(result.length===0, result);
});
test("Test all is resolved", function(){
   var smallerGroup = createSudokuSquaresFromArrayOfNumbers([1,2,3]);
    var largerGroup = createSudokuSquaresFromArrayOfNumbers([4,5,6,7,8,9]);
    var logic = SudokuLogic();
    var result = logic.findNumbersExistingInSmallGroupButNotInLargeGroup(smallerGroup, largerGroup);
    ok(result.length===3, result);

});

test("Test removeValuesFromFirstGroupThatIsOnlyExistentInSecondGroupWithinUnion", function(){
    var logic = SudokuLogic();
    var firstGroup = [SudokuSquare(), SudokuSquare(),SudokuSquare(),SudokuSquare(), SudokuSquare(),SudokuSquare()];
    var secondGroup= [SudokuSquare([4,5]),SudokuSquare([4,5]),SudokuSquare([5,6]),SudokuSquare([6,7,8]),SudokuSquare([8,9])];
    var union = [SudokuSquare([1,2]), SudokuSquare([2,3]),SudokuSquare([1,3])];
    var result = logic.removeValuesFromFirstGroupThatIsOnlyExistentInSecondGroupWithinUnion(firstGroup, secondGroup, union);
    ok(firstGroup[0].getPossibleValues().toString() === [4,5,6,7,8,9].toString(), firstGroup[0].getPossibleValues().toString());
});