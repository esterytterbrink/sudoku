/**
 * Created by esterytterbrink on 17/02/2014.
 */
function reduceGroupToString(quadrant){
    return quadrant.reduce(function(ack, curr){
            return ack + curr.resolvedValue();
        },""
    );
}