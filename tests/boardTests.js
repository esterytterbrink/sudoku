/**
 * Created by esterytterbrink on 15/02/2014.
 */

/**Boards*/
 var BOARDS = {
        easyBoard:[6,0,2,0,3,1,5,0,9,0,7,0,0,0,0,0,4,0,8,0,0,4,6,0,0,0,2,3,0,0,6,0,5,2,1,0,2,0,7,0,0,0,6,0,4,0,0,1,2,0,7,0,0,8,1,0,0,0,9,6,0,0,3,0,5,0,0,0,0,0,6,0,4,0,6,1,7,0,9,0,5],
      easiestBoard:[6,0,2,0,3,1,5,0,9,0,7,0,8,5,2,1,4,6,8,1,5,4,6,9,7,3,2,3,9,4,6,8,5,2,1,7,2,8,7,9,1,3,6,5,4,5,6,1,2,4,7,3,9,8,1,2,8,5,9,6,4,7,3,7,5,9,3,2,4,8,6,1,4,3,6,1,7,8,9,2,5],
      easierBoard:[6,0,2,0,3,1,5,0,9,0,7,0,0,5,2,1,4,6,8,1,5,4,6,9,7,3,2,3,9,4,6,8,5,2,1,7,2,8,7,9,1,3,6,5,4,5,6,1,2,4,7,3,9,8,1,2,8,5,9,6,4,7,3,7,5,9,3,2,4,8,6,1,4,3,6,1,7,8,9,2,5],
        easyFacit:[6,4,2,7,3,1,5,8,9,9,7,3,8,5,2,1,4,6,8,1,5,4,6,9,7,3,2,3,9,4,6,8,5,2,1,7,2,8,7,9,1,3,6,5,4,5,6,1,2,4,7,3,9,8,1,2,8,5,9,6,4,7,3,7,5,9,3,2,4,8,6,1,4,3,6,1,7,8,9,2,5],
     extremeBoard:[0,0,9,7,4,8,0,0,0,7,0,0,0,0,0,0,0,0,0,2,0,1,0,9,0,0,0,0,0,7,0,0,0,2,4,0,0,6,4,0,1,0,5,9,0,0,9,8,0,0,0,3,0,0,0,0,0,8,0,3,0,2,0,0,0,0,0,0,0,0,0,6,0,0,0,2,7,5,9,0,0],
     extremeFacit:[5,1,9,7,4,8,6,3,2,7,8,3,6,5,2,4,1,9,4,2,6,1,3,9,8,7,5,3,5,7,9,8,6,2,4,1,2,6,4,3,1,7,5,9,8,1,9,8,5,2,4,3,6,7,9,7,5,8,6,3,1,2,4,8,3,2,4,9,1,7,5,6,6,4,1,2,7,5,9,8,3],
           tbEasy:[5,1,0,0,0,0,0,8,3,8,0,0,4,1,6,0,0,5,0,0,0,0,0,0,0,0,0,0,9,8,5,0,4,6,1,0,0,0,0,9,0,1,0,0,0,0,6,4,2,0,3,5,7,0,0,0,0,0,0,0,0,0,0,6,0,0,1,5,7,0,0,4,7,8,0,0,0,0,0,9,6],
      tbEasyFacit:[5,1,6,7,2,9,4,8,3,8,7,3,4,1,6,9,2,5,9,4,2,8,3,5,7,6,1,3,9,8,5,7,4,6,1,2,2,5,7,9,6,1,3,4,8,1,6,4,2,8,3,5,7,9,4,3,1,6,9,8,2,5,7,6,2,9,1,5,7,8,3,4,7,8,5,3,4,2,1,9,6],
         tbMedium:[7,0,0,0,9,0,0,0,3,2,0,0,4,6,8,0,0,1,0,0,8,0,0,0,6,0,0,0,4,0,0,2,0,0,9,0,0,0,0,3,0,4,0,0,0,0,8,0,0,1,0,0,3,0,0,0,9,0,0,0,7,0,0,5,0,0,1,4,2,0,0,6,8,0,0,0,5,0,0,0,2],
    tbMediumFacit:[7,5,6,2,9,1,8,4,3,2,9,3,4,6,8,5,7,1,4,1,8,5,7,3,6,2,9,3,4,5,6,2,7,1,9,8,9,7,1,3,8,4,2,6,5,6,8,2,9,1,5,4,3,7,1,2,9,8,3,6,7,5,4,5,3,7,1,4,2,9,8,6,8,6,4,7,5,9,3,1,2],
           tbHard:[0,5,2,3,0,0,6,0,0,6,0,0,0,4,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,6,3,0,0,1,0,4,7,0,0,0,0,0,3,5,0,2,0,0,5,8,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,9,0,0,0,6,0,0,5,0,0,1,7,2,0],
      tbHardFacit:[7,5,2,3,8,9,6,4,1,6,1,9,7,4,5,2,8,3,8,4,3,1,2,6,5,7,9,5,9,8,6,3,7,4,1,2,4,7,6,9,1,2,8,3,5,3,2,1,4,5,8,9,6,7,2,6,4,5,7,3,1,9,8,1,8,7,2,9,4,3,5,6,9,3,5,8,6,1,7,2,4]

};

test("TestEasyBoard", function(){
    var board = SudokuBoard(BOARDS.easyBoard);
    var boardRepresentation = board.arrayRepresentation();
    deepEqual(boardRepresentation, BOARDS.easyFacit , boardRepresentation);

});

test("Test get row", function(){
   var board = SudokuBoard(BOARDS.easyFacit);
    var row = board.squaresForRow(0);
    var rowAsString = reduceGroupToString(row);
    ok(rowAsString === "642731589", rowAsString);
});

test("Test get row 5", function(){
    var board = SudokuBoard(BOARDS.easyFacit);
    var row = board.squaresForRow(4);
    var rowAsString = reduceGroupToString(row);
    ok(rowAsString === "287913654", rowAsString);
});

test("Test get quadrant8", function(){
    var board = SudokuBoard(BOARDS.easyFacit);
    var quadrant = board.squaresForQuadrant(8);
    var rowAsString = reduceGroupToString(quadrant);
    ok(rowAsString === "473861925", rowAsString);
});
test("Test get quadrant0", function(){
    var board = SudokuBoard(BOARDS.easyFacit);
    var quadrant = board.squaresForQuadrant(0);
    var rowAsString = reduceGroupToString(quadrant);
    ok(rowAsString === "642973815", rowAsString);
});
test("Test get quadrant1", function(){
    var board = SudokuBoard(BOARDS.easyFacit);
    var quadrant = board.squaresForQuadrant(1);
    var rowAsString = reduceGroupToString(quadrant);
    ok(rowAsString === "731852469", rowAsString);
});
test("Test get quadrant2", function(){
    var board = SudokuBoard(BOARDS.easyFacit);
    var quadrant = board.squaresForQuadrant(2);
    var rowAsString = reduceGroupToString(quadrant);
    ok(rowAsString === "589146732", rowAsString);
});
test("Test get quadrant3", function(){
    var board = SudokuBoard(BOARDS.easyFacit);
    var quadrant = board.squaresForQuadrant(3);
    var rowAsString = reduceGroupToString(quadrant);
    ok(rowAsString === "394287561", rowAsString);
});


test("Test get column", function(){
    var board = SudokuBoard(BOARDS.easyFacit);
    var row = board.squaresForColumn(0);
    var rowAsString = reduceGroupToString(row);
    ok(rowAsString === "698325174", rowAsString);
});

/**  subrowInQuadrant*/
test("Test get subrow from quadrant", function(){
   var board = SudokuBoard(BOARDS.easyFacit);
   var subrow = board.subrowInQuadrant(1,5);
   var rowAsString = reduceGroupToString(subrow);
    ok(rowAsString === "654", rowAsString);
});

/** subcolInQuadrant */
test("Test get subcol from quadrant", function(){
   var board = SudokuBoard(BOARDS.easyFacit);
   var subCol = board.subcolInQuadrant(0,5);
   var subColAsString = reduceGroupToString(subCol);
   ok(subColAsString==="263",subColAsString);

});

/**  restOfRowExceptSubpart*/
test("Test get rest of row", function(){
    var board = SudokuBoard(BOARDS.easyFacit);
    ok(reduceGroupToString(board.restOfRowExceptSubpart(7,2)) === "759324", "7,2");
    ok(reduceGroupToString(board.restOfRowExceptSubpart(0,0)) === "731589", "0,0");
    ok(reduceGroupToString(board.restOfRowExceptSubpart(8,1)) === "436925", "8,1");
    ok(reduceGroupToString(board.restOfRowExceptSubpart(4,1)) === "287654", "7,2");

});

/**  restOfColExceptSubpart*/
test("Test get rest of column except subpart", function(){
   var board = SudokuBoard(BOARDS.easyFacit);
    var restOfCol = board.restOfColExceptSubpart(3,1);
    var restOfColAsString = reduceGroupToString(restOfCol);
    ok(restOfColAsString ==="784531", restOfColAsString);
});

/**  restOfQuadrantExceptOfSubrow*/
test("Test get rest of quadrant except subrow", function(){
    var board = SudokuBoard(BOARDS.easyFacit);
    var restOfCol = board.restOfQuadrantExceptOfSubrow(3,1);
    var restOfColAsString = reduceGroupToString(restOfCol);
    ok(restOfColAsString ==="394561", restOfColAsString);
});

/**  restOfQuadrantExceptOfSubcol*/
test("Test get rest of quadrant except subcol", function(){
    var board = SudokuBoard(BOARDS.easyFacit);
    var restOfCol = board.restOfQuadrantExceptOfSubcol(7,1);
    var restOfColAsString = reduceGroupToString(restOfCol);
    ok(restOfColAsString ==="563418", restOfColAsString);
});

/**   rowIndexForRowIndexInQuadrant*/
test("Test row index for row index in quadrant", function(){
    var board = SudokuBoard(BOARDS.easyFacit);
    var indexes = [];
    for(var quadrant = 0; quadrant <9; quadrant ++){
        for(var row= 0; row<3; row ++){
            indexes.push(board.rowIndexForRowIndexInQuadrant(row,quadrant));
        }
    }
    var facit = [0,1,2,0,1,2,0,1,2,3,4,5,3,4,5,3,4,5,6,7,8,6,7,8,6,7,8];
    ok(indexes.toString() === facit.toString(), indexes.toString());
});

/**   columnIndexForColumnIndexInQuadrant*/
test("Test column index for quadrant", function(){
    var board = SudokuBoard(BOARDS.easyFacit);
    var indexes = [];
    for(var column= 0; column<3; column ++){
        for(var quadrant = 0; quadrant <9; quadrant ++){
            indexes.push(board.columnIndexForColumnIndexInQuadrant(column,quadrant));
        }
    }
    var facit = [0,3,6,0,3,6,0,3,6,1,4,7,1,4,7,1,4,7,2,5,8,2,5,8,2,5,8];
    ok(indexes.toString() === facit.toString(), indexes.toString());
});

test("Test start index for quadrant", function(){
    var board = SudokuBoard(BOARDS.easyFacit);
    ok(board.startIndexForQuadrant(0) === 0, board.startIndexForQuadrant(0));
    ok(board.startIndexForQuadrant(1) === 3, board.startIndexForQuadrant(1));
    ok(board.startIndexForQuadrant(2) === 6, board.startIndexForQuadrant(2));
    ok(board.startIndexForQuadrant(3) === 27, board.startIndexForQuadrant(3));
    ok(board.startIndexForQuadrant(4) === 30, board.startIndexForQuadrant(4));
    ok(board.startIndexForQuadrant(5) === 33, board.startIndexForQuadrant(5));
    ok(board.startIndexForQuadrant(6) === 54, board.startIndexForQuadrant(6));
    ok(board.startIndexForQuadrant(7) === 57, board.startIndexForQuadrant(7));
    ok(board.startIndexForQuadrant(8) === 60, board.startIndexForQuadrant(8));

});

test("Test solve easier board", function(){
    var board = SudokuBoard(BOARDS.easierBoard);
    var boardRepresentation = board.arrayRepresentation();
    ok(_.isEqual(boardRepresentation, BOARDS.easyFacit), board.squaresAsArray[3].getPossibleValues());

});

test("Test solve easiest board", function(){
    var board = SudokuBoard(BOARDS.easiestBoard);
    var boardRepresentation = board.arrayRepresentation();
    ok(_.isEqual(boardRepresentation, BOARDS.easyFacit), boardRepresentation);

});

test("Test solve easy board", function(){
    var board = SudokuBoard(BOARDS.easyBoard);
    var boardRepresentation = board.arrayRepresentation();
    ok(_.isEqual(boardRepresentation, BOARDS.easyFacit), boardRepresentation);

});
test("Test solve Trialbee easy board", function(){
    var board = SudokuBoard(BOARDS.tbEasy);
    var boardRepresentation = board.arrayRepresentation();
    ok(_.isEqual(boardRepresentation, BOARDS.tbEasyFacit), boardRepresentation);

});
test("Test solve Trialbee medium board", function(){
    var board = SudokuBoard(BOARDS.tbMedium);
    board.solve();
    var boardRepresentation = board.arrayRepresentation();
    ok(_.isEqual(boardRepresentation, BOARDS.tbMediumFacit), boardRepresentation);

});
test("Test solve Trialbee hard board", function(){
    var board = SudokuBoard(BOARDS.tbHard);
    board.solve();
    var boardRepresentation = board.arrayRepresentation();
    deepEqual(boardRepresentation, BOARDS.tbHardFacit, boardRepresentation);

});