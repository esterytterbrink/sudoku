/**
 * Created by esterytterbrink on 15/02/2014.
 */

test("Test possibleValues", function(){
    var square1 = SudokuSquare([1]);
    var square2 = SudokuSquare([2]);
    var square3 = SudokuSquare([3]);
   var subRow = SudokuSubRow(square1, square2, square3);
    ok(subRow.getSubRowPossibleValues().toString() == "1,2,3", square1.getPossibleValues() + square2.getPossibleValues() + square3.getPossibleValues());

});

test("Test more complex possibleValues", function(){
    var square1 = SudokuSquare([1, 2]);
    var square2 = SudokuSquare([2,5]);
    var square3 = SudokuSquare([3,7]);
    var subRow = SudokuSubRow(square1, square2, square3);
    ok(subRow.getSubRowPossibleValues().toString() == "1,2,5,3,7", square1.getPossibleValues() + square2.getPossibleValues() + square3.getPossibleValues());

});

test("Test simple confirmedValues", function(){
    var square1 = SudokuSquare([9]);
    var square2 = SudokuSquare([2,5]);
    var square3 = SudokuSquare([3,7]);
    var subRow = SudokuSubRow(square1, square2, square3);
    ok(subRow.getConfirmedValues().toString() == "9",subRow.getConfirmedValues().toString() );

});

test("Test more complex confirmedValues", function(){
    var square1 = SudokuSquare([9]);
    var square2 = SudokuSquare([2,5]);
    var square3 = SudokuSquare([2,7]);
    var subRow = SudokuSubRow(square1, square2, square3);
    subRow.confirmValue(2);
    ok(subRow.getConfirmedValues().toString() == "9,2",subRow.getConfirmedValues().toString() );

});